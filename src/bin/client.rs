use std::net::SocketAddr;
use laminar::{Socket, Packet, SocketEvent};
use piglog::prelude::*;
use cli_input::prelude::*;
use colored::Colorize;

fn main() {
    client().unwrap();
}

fn client() -> Result<(), laminar::ErrorKind> {
    piglog::info!("Running as client...");

    let client_ip = input!("{} ", "(You) Client IP:".bright_cyan().bold());
    let server_ip = input!("{} ", "Server IP:".bright_cyan().bold());

    piglog::info!("Setting up client on: {}", client_ip.bright_green().bold());

    let mut socket = Socket::bind(&client_ip)?;

    piglog::success!("Client on: {}", client_ip.bright_green().bold());

    let server: SocketAddr = server_ip.parse().unwrap();

    piglog::info!("Waiting for server to send relay address...");

    loop {
        socket.send(Packet::reliable_unordered(server, "relay please".as_bytes().to_vec())).unwrap();

        socket.manual_poll(std::time::Instant::now());

        match socket.recv() {
            Some(SocketEvent::Packet(packet)) => {
                if packet.addr() == server {
                    let received = String::from_utf8_lossy(packet.payload());

                    let segments: Vec<&str> = received.trim().split(" ").collect();

                    if segments.len() == 2 {
                        if segments[0] == "relay" {
                            std::env::set_var("RELAY_ADDRESS", segments[1]);

                            break;
                        }
                    }
                }
            },
            _ => (),
        };

        std::thread::sleep(std::time::Duration::from_secs(1));
    }

    let relay_ip = std::env::var("RELAY_ADDRESS").unwrap();

    piglog::success!("Received relay address: {}", relay_ip);

    return Ok(());
}
