use std::net::SocketAddr;
use laminar::{Socket, SocketEvent, Packet};
use piglog::prelude::*;
use cli_input::prelude::*;
use colored::Colorize;

fn main() {
    relay().unwrap();
}

fn relay() -> Result<(), laminar::ErrorKind> {
    piglog::info!("Running as relay...");

    let relay_ip = input!("{} ", "(You) Relay IP:".bright_cyan().bold());
    let server_ip = input!("{} ", "Server IP:".bright_cyan().bold());
    let server_password = sha256::digest(input_hidden!("{} ", "Server Password:".bright_cyan().bold()));

    piglog::info!("Setting up relay on: {}", relay_ip.bright_green().bold());

    let mut socket = Socket::bind(&relay_ip)?;

    let sender = socket.get_packet_sender();
    let receiver = socket.get_event_receiver();

    let _ = std::thread::spawn(move || socket.start_polling());

    piglog::success!("Relay on: {}", relay_ip.bright_green().bold());

    let server: SocketAddr = server_ip.parse().unwrap();

    loop {
        match sender.send(Packet::reliable_unordered(server, format!("new_relay {} {}", relay_ip, server_password).into_bytes())) {
            Err(_) => {
                piglog::error!("Failed to send self to server!");
            },
            _ => break,
        };
    }

    loop {
        if let Ok(event) = receiver.recv() {
            match event {
                SocketEvent::Packet(packet) => {
                    let mut prefix = packet.addr().to_string();

                    let mut from_server = false;

                    if prefix == server_ip {
                        prefix = String::from("Server");
                        from_server = true;
                    }

                    let msg = String::from_utf8_lossy(packet.payload());

                    if from_server && msg == "relay denied" {
                        piglog::fatal!("Server denied relay connection!");
                        std::process::exit(1);
                    }
                    
                    else if from_server && msg == "relay allowed" {
                        piglog::success!("Registered as relay on server!");
                    }
                    
                    else {
                        println!("{l}{}{r} {}", prefix, msg, l = "(".bright_black().bold(), r = "):".bright_black().bold());
                    }
                },
                _ => (),
            };
        }
    }
}
