use laminar::{Socket, SocketEvent, Packet};
use piglog::prelude::*;
use cli_input::prelude::*;
use colored::Colorize;

fn main() {
    server().unwrap();
}

fn server() -> Result<(), laminar::ErrorKind> {
    let mut relays: Vec<String> = Vec::new();

    piglog::info!("Running as server...");

    let server_ip = input!("{} ", "(You) Server IP:".bright_cyan().bold());
    let password = sha256::digest(input_hidden!("{} ", "Create Server Password:".bright_cyan().bold()));

    piglog::info!("Setting up server on: {}", server_ip.bright_green().bold());

    let mut socket = Socket::bind(&server_ip)?;

    let sender = socket.get_packet_sender();
    let receiver = socket.get_event_receiver();

    let _ = std::thread::spawn(move || socket.start_polling());

    piglog::success!("Server on: {}", server_ip.bright_green().bold());

    loop {
        if let Ok(event) = receiver.recv() {
            match event {
                SocketEvent::Packet(packet) => {
                    let msg = String::from_utf8_lossy(packet.payload());

                    println!("{l}{}{r} {}", packet.addr().to_string().bright_green().bold(), msg, l = "(".bright_black().bold(), r = "):".bright_black().bold());

                    if msg == "relay please" {
                        if relays.len() > 0 {
                            let relay_address: String = relays[0].to_string();

                            match sender.send(Packet::reliable_unordered(packet.addr(), format!("relay {}", relay_address).into_bytes())) {
                                Ok(_) => (),
                                Err(_) => {
                                    piglog::error!("Failed to send relay address to '{}'!", packet.addr());
                                },
                            };
                        }
                    }

                    else if msg.starts_with("new_relay") {
                        let segments: Vec<&str> = msg.trim().split(" ").collect();

                        let relay_address = segments[1];
                        let password_from_relay = segments[2];

                        if password_from_relay == password {
                            piglog::info!("Adding relay: {}", relay_address);
                            relays.push(relay_address.to_string());
                            match sender.send(Packet::reliable_unordered(relay_address.parse().unwrap(), b"relay allowed".to_vec())) {
                                Ok(_) => (),
                                Err(_) => {
                                    piglog::error!("Failed to send message to relay!");
                                },
                            };
                        } else {
                            piglog::warning!("Relay '{}' was denied: wrong password!", relay_address);
                            match sender.send(Packet::reliable_unordered(relay_address.parse().unwrap(), b"relay denied".to_vec())) {
                                Ok(_) => (),
                                Err(_) => {
                                    piglog::error!("Failed to send message to relay!");
                                },
                            };
                        }
                    }
                },
                _ => (),
            };
        }
    }
}
