# Server Relay Client Example
This example is about how to use Laminar UDP to create servers, relays and clients.

# How To Use
Open 3 terminals, and run 1 cargo command from below in each terminal, in the project.
```
cargo run --bin server
cargo run --bin relay
cargo run --bin client
```
